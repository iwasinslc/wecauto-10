<?php
namespace App\Console\Commands\Manual;

use App\Events\ExchangeAllOperations;
use App\Events\Message;
use App\Events\NotificationEvent;
use App\Events\Test;
use App\Jobs\AccrueDeposit;
use App\Jobs\CloseDeposit;
use App\Models\Currency;
use App\Models\Deposit;
use App\Models\ExchangeOrder;
use App\Models\Licences;
use App\Models\OrderRequest;
use App\Models\PaymentSystem;
use App\Models\Rate;
use App\Models\Transaction;
use App\Models\TransactionType;
use App\Models\User;
use App\Models\UserRank;
use App\Modules\PaymentSystems\EtherApiModule;
use Carbon\Carbon;
use DB;
use Illuminate\Console\Command;

/**
 * Class TestFibonacci
 * @package App\Console\Commands\Manual
 */
class TestEvents extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:events';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check all users balances.';

    /** @var string $userId */
    protected $userId;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

       $users = User::query()->whereHas('exchangeOrders', function ($q) {
           $q->where('created_at', '>=', '2020-10-21 12:23:00');
       })->get();

       var_dump($users->count());



    }



}
