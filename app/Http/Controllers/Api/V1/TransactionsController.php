<?php


namespace App\Http\Controllers\Api\V1;


use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RequestDataTable;
use App\Http\Requests\Api\RequestTransactionTicketCreate;
use App\Http\Resources\TransactionResource;
use App\Models\Transaction;
use App\Models\Wallet;
use Illuminate\Http\Response;

class TransactionsController extends Controller
{

    /**
     * @OA\Get(
     *      path="/transactions",
     *      operationId="getTransactions",
     *      tags={"Transactions"},
     *      summary="Get list of the transactions",
     *      description="Return array of the transactions. Filters: user_id",
     *      @OA\Parameter(
     *          ref="#/components/parameters/user_id"
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/TransactionResource")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function index(RequestDataTable $request) {
        return TransactionResource::collection(
                Transaction::with(['currency', 'type'])
                    ->where('user_id', $request->user_id)->get()
            );
    }

    /**
     * @OA\POST(
     *      path="/transactions/{type}",
     *      operationId="createTransactions",
     *      tags={"Transactions"},
     *      summary="Create ticket for transaction",
     *      description="Create transaction. But this transaction must be approved by admin from admin panel.",
     *      @OA\Parameter(
     *          name="type",
     *          description="Transaction type. For now: bonus, penalty",
     *          required=true,
     *          example="bonus",
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/RequestTransactionTicketCreate")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation"
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function storeTicket(RequestTransactionTicketCreate $requestTransactionCreate, $transaction_type) {
        /** @var Wallet $wallet */
        $wallet = Wallet::find($requestTransactionCreate->wallet_id);
        if(empty($wallet)) abort(Response::HTTP_NOT_FOUND, 'Wallet not found');
        switch($transaction_type) {
            case 'bonus':
                $wallet->addBonus($requestTransactionCreate->value, [
                    'batch_id' => $requestTransactionCreate->batch_id ?? null,
                    'source' => $requestTransactionCreate->comment
                ]);
                break;
            case 'penalty':
                $wallet->makePenalty(
                    $requestTransactionCreate->value,
                    $requestTransactionCreate->comment,
                    true
                );
                break;
            default:
                abort(Response::HTTP_BAD_REQUEST);
        }
        return response(['message' => 'Transaction request created'], Response::HTTP_CREATED);
    }

}