<?php
namespace App\Models\Youtube;

use App\Models\User;
use App\Traits\ModelTrait;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

/**
 * Class YoutubeVideoWatch
 * @package App\Models\Youtube
 *
 * @property string resource_url
 * @property User user_id
 */
class YoutubeVideoWatch extends Model
{
    use ModelTrait;
    use Uuids;

    /** @var string $table */
    protected $table = 'youtube_video_watch';

    /** @var bool $incrementing */
    public $incrementing = false;
    protected $keyType = 'string';

    /** @var array $fillable */
    protected $fillable = [
        'resource_url',
        'user_id',
    ];

    /** @var array $timestamps */
    public $timestamps = ['created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
