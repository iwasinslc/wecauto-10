<?php
namespace App\Observers;

use App\Models\ProjectLikes;

/**
 * Class ProjectLikesObserver
 * @package App\Observers
 */
class ProjectLikesObserver
{
    /**
     * @param ProjectLikes $projectLike
     */
    public function deleting(ProjectLikes $projectLike)
    {
        // ..
    }

    /**
     * @param ProjectLikes $projectLike
     * @return array
     */
    private function getCacheKeys(ProjectLikes $projectLike): array
    {
        return [];
    }

    /**
     * @param ProjectLikes $projectLike
     * @return array
     */
    private function getCacheTags(ProjectLikes $projectLike): array
    {
        return [];
    }

    /**
     * Listen to the ProjectLikes created event.
     *
     * @param ProjectLikes $projectLike
     * @return void
     * @throws
     */
    public function created(ProjectLikes $projectLike)
    {
        clearCacheByArray($this->getCacheKeys($projectLike));
        clearCacheByTags($this->getCacheTags($projectLike));
    }

    /**
     * Listen to the ProjectLikes deleting event.
     *
     * @param ProjectLikes $projectLike
     * @return void
     * @throws
     */
    public function deleted(ProjectLikes $projectLike)
    {
        clearCacheByArray($this->getCacheKeys($projectLike));
        clearCacheByTags($this->getCacheTags($projectLike));
    }

    /**
     * Listen to the ProjectLikes updating event.
     *
     * @param ProjectLikes $projectLike
     * @return void
     * @throws
     */
    public function updated(ProjectLikes $projectLike)
    {
        clearCacheByArray($this->getCacheKeys($projectLike));
        clearCacheByTags($this->getCacheTags($projectLike));
    }
}