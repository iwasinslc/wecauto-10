<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

/**
 * Class RolesAndPermissionsSeeder
 */
class RolesAndPermissionsSeeder extends Seeder
{
    public function run()
    {
        app()['cache']->forget('spatie.permission.cache');

        $roles = [
            'root',
            'admin',
            'writer',
            'moderator'
        ];

        foreach ($roles as $role) {
            if (Role::where('name', $role)->count() == 0) {
                Role::create(['name' => $role]);
                echo "Role '$role' registered.\n";
            } else {
                echo "Role 'root' already registered.\n";
            }
        }
    }
}
