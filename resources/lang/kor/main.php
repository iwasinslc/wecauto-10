<?php
return [
    'emails' => [
        'spam_protection'   => '메일이 발송되지 않았습니다. 스팸 규칙입니다.',
        'sent_successfully' => '메일이 성공적으로 전송되었습니다',
        'unable_to_send'    => '이 메일을 보내는 것은 불가능합니다',

        'request' => [
            'email_required' => 'Email 필드가 필요합니다.',
            'email_max'      => '최대 이메일 길이는 255자입니다',
            'email_email'    => '이메일 형식이 잘못되었습니다!',

            'text_required' => 'Text 필드가 필요합니다.',
            'text_min'      => '최소 텍스트 길이는 10자입니다',
        ],
    ],
    'transaction_types' => [
        'enter'      => '잔액의 보충',
        'withdraw'   => '자금인출',
        'bonus'      => '보너스',
        'partner'    => '파트너 수수료',
        'dividend'   => '예금수익',
        'create_dep' => '예금창조',
        'close_dep'  => '예금마감',
        'penalty'    => '과태료',
    ],

];
